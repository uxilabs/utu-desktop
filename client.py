# -*- coding: UTF-8 -*-

from gi.repository import Gtk
import json


class Client(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)

        vbox_main = Gtk.HBox()
        self.add(vbox_main)

        toolbar = Gtk.Toolbar()
        toolbar.set_orientation(Gtk.Orientation.VERTICAL)

        notebook = Gtk.Notebook()
        notebook.append_page(Horarios, Gtk.Label("Horarios"))

        self.show_all()


class Horarios(Gtk.VBox):
    def __init__(self):
        Gtk.VBox.__init__(self)

        groups = json.load("data/groups.json")

        self.combo_groups = Gtk.ComboBox()

        groups.sort()
        for group in groups:
            self.combo_groups.append_text(group.replace(".", "°"))

        box_aux = Gtk.HBox()
        self.pack_start(box_aux, False, False)

        title = Gtk.Label()
        title.set_markup("<b>Horarios de la semana</b>")

        box_aux.pack_start(title, False, False, 5)
        box_aux.pack_end(self.combo_groups, False, False, 5)

        notebook_days = Gtk.Notebook()
        store = self.create_model()
        treeView = gtk.TreeView(store)

        self.create_columns(treeView)

        notebook_days.append_page(day, treeView)

    def create_model(self):
        testdata = [(1, 'Mihai', 'Ion'), (2, 'John', 'Doe'), (3, 'Silvester', 'Rambo')]
        store = gtk.ListStore(int, str, str)
        for item in testdata:
            store.append([item[0], item[1], item[2]])

        return store

    def create_columns(self, treeView):
        days = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]

        for day in days:
            rendererText = gtk.CellRendererText()
            column = gtk.TreeViewColumn(day, rendererText, text=0)
            column.set_sort_column_id(0)
            treeView.append_column(column)


class VBox_Materias(Gtk.TreeView):
    def __init__(self, group, day):
        Gtk.VBox__init__(self)

        store = self.create_model()
        treeView = gtk.TreeView(store)

        horarios = json.load("data/horarios.json")
        if group in horarios:
            if day in horarios[group]:
                materias = horarios[group][day]

                for materia in materias:
                    print materia
        else:
            print "No hay datos!"

        self.create_columns(treeView)
        notebook_days.append_page(day, treeView)

    def create_model(self):
        testdata = json.load("data/horarios.json")
        store = gtk.ListStore(str, str, str, str, str, str)
        for materia in testdata:
            item = testdata[materia]
            store.append(materia, item["prof"], item["salon"], item["inicio"], item["final"])

        return store

    def create_columns(self, treeView):
        columns = ["Materia", "Profesor", "Salón", "Entrada", "Salida"]

        for columna in columns:
            rendererText = gtk.CellRendererText()
            column = gtk.TreeViewColumn(columna, rendererText, text=0)
            column.set_sort_column_id(0)
            treeView.append_column(column)

Client()
Gtk.main()
